# Поддержка PRO

Плагин Поддержка PRO расширяет возможности приложения Поддержка.

### Вам станут доступны
- Мобильная версия всего приложения 
- Возможность создания новых запросов из сообщений
- Избранные сообщения
- Вложения
- Спам лист
- Конструктор полей контакта
- Конструктор дополнительных полей запроса
- Быстрый просмотр изображений
- Редактирование дополнительных полей
- Изменение цвета запросов
- Теги CRM
- Теги в списке запросов
- Разработчикам Вебасист 
 
Если Вы официальный разработчик Вебасист, то для Вас создан инструмент для проверки лицензий по номеру заказа или домену. Обрабатывайте запросы и сразу же проверяйте наличие лицензий на одной странице.

### Обновления и новый функционал

Если у вас есть пожелания по улучшению продукта, будем рады рассмотреть ваши MR (Merge requests).